
import urllib3
from flask import Flask
import rsa_archer
from rsa_archer.archer_instance import ArcherInstance
import requests
import json

app = Flask(__name__)


@app.route('/servicioweb')
def servicioWeb():
	try:
		urllib3.disable_warnings()
		resultado = ""
		archer_instance = ArcherInstance(
			"sandbox6.archer.rsa.com",
			"64005",
			"rogelionava",
			"PruebasGentera."
		)

		archer_instance.get_session_token()

		archer_instance.from_application("Vulnerability Library")
		# print(
		# 	"\n\narcher: {}\n\n".format(archer_instance.__dict__)
		# )
		#test = archer_instance.get_value_id_by_field_name_and_value('Source', 'Symantec')
		print(
			"\n\narcher: {}\n\n".format(archer_instance.__dict__)
		)
		record_json = {
			"ID": "4221797",
			"Title": "Insert from Python 17",
			"Source": ["Symantec"]
		}
		record_id   = archer_instance.create_content_record(record_json)
		# print(
		# 	"\n\record_id: {}\n\n".format(record_id)
		# )		
		# header = {
		# 	"Accept": "application/json,text/html,application/xhtml+xml,application/xml;q =0.9,*/*;q=0.8",
		# 	"Content-type": "application/json"
		# }
		# response = requests.post(
		# 	'https://sandbox6.archer.rsa.com/platformapi/core/security/login', 
		# 	headers=header, 
		# 	json={
		# 			"InstanceName": 64005,
		# 			"Username"    : "rogelionava", 
		# 			"UserDomain"  : "",
		# 			"Password"    : "PruebasGentera."
		# 		}, 
		# 	verify=False)
		# data = json.loads(response.content.decode("utf-8"))
		print("\n\nrecord_id: {}\n\n".format(record_id))
	except Exception as ex:
		
		print(
				print("\n\nError: {}\n\n".format(ex))
			)

	finally:
		print("\n\n\nTermina servicio web\n\n\n")
	
	return 'Termino la ejecucion'
	
if __name__ == '__main__':
	app.run('127.0.0.1', 8000)



"""
import urllib3
import requests
import json

urllib3.disable_warnings()


header = {
	"Accept": "application/json,text/html,application/xhtml+xml,application/xml;q =0.9,*/*;q=0.8",
	"Content-type": "application/json"
}
response = requests.post(
	'https://sandbox6.archer.rsa.com/core/security/login', 
	headers=header, 
	json={
			"InstanceName": 64005,
			"Username"    : "rogelionava", 
			"UserDomain"  : "",
			"Password"    : "PruebasGentera."
		}, 
	verify=False)


#data = json.loads(response.content.decode("utf-8"))
#a = response.__dict__


print("veamos data: {}".format(response.connection.__dict__))
"""